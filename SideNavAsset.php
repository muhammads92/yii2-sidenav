<?php

/**
 * @copyright Copyright &copy; Muhammad Samadov, Krajee.com, 2014
 * @package yii2-widgets
 * @subpackage yii2-widget-sidenav
 * @version 1.0.0
 */

namespace muhammmad\sidenav;

/**
 * Asset bundle for SideNav Widget
 *
 * @author Muhammad Samadov <sam.muhammadali@gmail.com>
 * @since 1.0
 */
class SideNavAsset extends \yii\web\AssetBundle
{
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/sidenav']);
        $this->setupAssets('js', ['js/sidenav']);
        parent::init();
    }
}
